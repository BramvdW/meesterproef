﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InterfacesLogic
{
    public interface IVerkiezing
    {
        int Id { get; }
        int Stemmen { get; }
        decimal Percentage { get; set; }
        int Zetels { get; set; }
        string OrdeTrekker { get; }
        IPartij Partij { get; }
    }
}
