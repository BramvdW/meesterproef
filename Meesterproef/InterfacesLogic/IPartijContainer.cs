﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InterfacesLogic
{
    public interface IPartijContainer
    {
        string MaakPartijAan(IPartij partij);

        List<IPartij> HaalAllePartijenOp();
    }
}
