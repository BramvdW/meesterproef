﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InterfacesLogic
{
    public interface IPartij
    {
        int Id { get; }
        string Orde { get; }
        string VoledigeNaam { get; }

        string PartijAanpassen();
    }
}
