﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InterfacesLogic
{
    public interface IUitslag
    {
        int Id { get; }
        string Naam { get; }
        DateTime Datum { get; }
        int ZetelAantal { get; }
        List<IVerkiezing> Verkiezingen { get; }

        string UitslagAanpassen();
    }
}
