﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InterfacesLogic
{
    public interface ICoalitie
    {
        string Naam { get; }

        List<IVerkiezing> verkiezingen { get; }

        void VoegVerkiezingToe(IVerkiezing verkiezing);
    }
}
