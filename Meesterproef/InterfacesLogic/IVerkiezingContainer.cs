﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InterfacesLogic
{
    public interface IVerkiezingContainer
    {
        IVerkiezing HaalVerkiezingOp(int verkiezingId);
    }
}
