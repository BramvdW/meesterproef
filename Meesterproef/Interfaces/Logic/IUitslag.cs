﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interfaces.Logic
{
    public interface IUitslag
    {
        int Id { get; }
        string Naam { get; }
        DateTime Datum { get; }
        int ZetelAantal { get; }
        List<IVerkiezing> Verkiezingen { get; }

        string UitslagAanpassen(int uitslagId);
    }
}
