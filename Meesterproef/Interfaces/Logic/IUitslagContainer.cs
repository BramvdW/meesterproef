﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interfaces.Logic
{
    public interface IUitslagContainer
    {
        string MaakUitslagAan(IUitslag uitslag);

        List<IUitslag> HaalAlleUitslagenOp();

        IUitslag HaalAlleDataVanUitslagOp(IUitslag uitslag);

        IUitslag HaalUitslagOpMetNaam(string uitslagNaam);
    }
}
