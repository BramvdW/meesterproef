﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interfaces.Logic
{
    public interface IVerkiezing
    {
        int Stemmen { get; }
        decimal Percentage { get; }
        int Zetels { get; }
        IPartij Partij { get; }
    }
}
