﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interfaces.Logic
{
    public interface IPartij
    {
        int Id { get; }
        string Orde { get; }
        string VoledigeNaam { get; }
        string OrdeTrekker { get; }
    }
}
