﻿using System;
using System.Collections.Generic;
using System.Text;
using Structs;

namespace Interfaces.Dal
{
    public interface IUitslag
    {
        void UitslagAanpassen(UitslagDTO uitslag, int uitslagId);
    }
}
