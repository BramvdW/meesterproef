﻿using System;
using System.Collections.Generic;
using System.Text;
using Structs;

namespace Interfaces.Dal
{
    public interface IUitslagContainer
    {
        void MaakUitslagAan(UitslagDTO uitslag);

        List<UitslagDTO> HaalAlleUitslagenOp();

        UitslagDTO HaalAlleDataVanUitslagOp(UitslagDTO uitslag);

        UitslagDTO HaalUitslagOpMetNaam(string uitslagNaam);
    }
}
