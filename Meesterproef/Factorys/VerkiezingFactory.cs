﻿using System;
using System.Collections.Generic;
using System.Text;
using DalLaag;
using IntefacesDal;

namespace FactorysDal
{
    public static class VerkiezingFactory
    {
        public static IVerkiezingContainer MaakUitslagContainer()
        {
            IVerkiezingContainer verkiezingContainer = new VerkiezingDal();
            return verkiezingContainer;
        }

        public static IVerkiezing MaakUitslag()
        {
            IVerkiezing verkiezing = new VerkiezingDal();
            return verkiezing;
        }
    }
}
