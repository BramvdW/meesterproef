﻿using System;
using System.Collections.Generic;
using System.Text;
using DalLaag;
using IntefacesDal;

namespace FactorysDal
{
    public static class PartijFactory
    {
        public static IPartij MaakPartij()
        {
            IPartij partij = new PartijDal();
            return partij;
        }

        public static IPartijContainer MaakPartijContainerAan()
        {
            IPartijContainer partij = new PartijDal();
            return partij;
        }

        
    }
}
