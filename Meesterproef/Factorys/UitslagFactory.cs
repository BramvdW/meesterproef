﻿using DalLaag;
using System;
using System.Collections.Generic;
using System.Text;
using InterfacesDal;

namespace FactorysDal
{
    public static class UitslagFactory
    {
        public static IUitslagContainer MaakUitslagContainer()
        {
            IUitslagContainer uitslagContainer = new UitslagDal();
            return uitslagContainer;
        }

        public static IUitslag MaakUitslag()
        {
            IUitslag uitslag = new UitslagDal();
            return uitslag;
        }
    }
}
