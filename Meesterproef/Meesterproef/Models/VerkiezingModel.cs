﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Meesterproef.Models
{
    public class VerkiezingModel
    {
        public int Stemmen { get; set; }
        public decimal Percentage { get; set; }
        public int Zetels { get; set; }
        public string OrdeTrekker { get; set; }
        public PartijModel Partij { get; set; }
    }
}
