﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
 using Microsoft.AspNetCore.Mvc.Rendering;

 namespace Meesterproef.Models
{
    public class UitslagModel
    {
        public int Id { get; set; }
        public string Naam { get; set; }
        public DateTime Datum { get; set; }
        public int ZetelAantal { get; set; }

        public List<VerkiezingModel> Verkiezingen { get; set; }
    }
}
