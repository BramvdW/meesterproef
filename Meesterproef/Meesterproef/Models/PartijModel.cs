﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Meesterproef.Models
{
    public class PartijModel
    {
        public int Id { get; set; }
        public string Orde { get; set; }
        public string VoledigeNaam { get; set; }
        
    }
}
