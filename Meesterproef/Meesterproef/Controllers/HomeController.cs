﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using FactorysLogic;
using InterfacesLogic;
using LogicaLaag;
using Microsoft.AspNetCore.Mvc;
using Meesterproef.Models;

namespace Meesterproef.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            string aanpasData = PartijFactory.MaakPartijAan(3, "", "OrdeTestPartij").PartijAanpassen();
            return View();
        }
    }
}
