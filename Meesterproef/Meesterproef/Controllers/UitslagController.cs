﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FactorysLogic;
using InterfacesLogic;
using LogicaLaag;
using Meesterproef.Models;
using Microsoft.AspNetCore.Mvc;
using Org.BouncyCastle.Bcpg;

namespace Meesterproef.Controllers
{

    public class UitslagController : Controller
    {
        

        public IActionResult ToevoegenUitslag()
        {
            return View();
        }

        [HttpPost]
        public IActionResult ToevoegenUitslag(UitslagModel uitslagModel)
        {
            IUitslag uitslag = UitslagFactory.MaakUitslag(uitslagModel.Id, uitslagModel.Naam, uitslagModel.Datum, uitslagModel.ZetelAantal);

            ViewBag.UitslagData = UitslagFactory.MaakUitslagContainer().MaakUitslagAan(uitslag);
            //voegt uitslag toe

            return View();
        }

        public IActionResult UitslagTonen()
        {
            List<IUitslag> uitslagen = UitslagFactory.MaakUitslagContainer().HaalAlleUitslagenOp();

            ViewBag.Uitslagen = uitslagen;
            //Haal alle uitslagen op

            return View();
        }

        [HttpPost]
        public IActionResult UitslagTonen(VerkiezingModel verkiezingModel, int uitslagId, string uitslagNaam, string orde, string volledigeNaam, string ordeTrekker, string submit,string clearCoalitie)
        {

            ICoalitie iCoalitie = CoalitieFactory.MaakLegeCoalitie();


            if (iCoalitie.verkiezingen.Count > 0)
            {

            }


            //party id en uitslag id de verkiezing ophalen


            //haalt alle uitslagen op om in het drop down menu te zetten
            List<IUitslag> uitslagen = UitslagFactory.MaakUitslagContainer().HaalAlleUitslagenOp();

            ViewBag.Uitslagen = uitslagen;


            //haalt een uitslag op
            IUitslag IUitslag = UitslagFactory.MaakUitslagContainer().HaalUitslagOpMetNaam(uitslagNaam.ToString());

            //Haal de alle data op van de gewenste uitslag met de naam van de uitslag
            IUitslag uitslag = UitslagFactory.MaakUitslagContainer().HaalAlleDataVanUitslagOp(IUitslag);

            if (submit != null)
            {
                //haalt de geselecteerde verkiezing op
                IVerkiezing iVerkiezing =
                    VerkiezingFactory.MaakVerkiezingContainer().HaalVerkiezingOp(Convert.ToInt32(submit));

                iCoalitie.verkiezingen.Add(iVerkiezing);
            }

            if (clearCoalitie == "clear")
            {
                iCoalitie.verkiezingen.Clear();
            }

            
            ViewBag.Coalitie = iCoalitie.verkiezingen;
            ViewBag.Uitslag = uitslag;
            ViewBag.Verkiezingen = uitslag.Verkiezingen;

            return View();
        }

        //geef de button als value het id van de verkiezing

        public IActionResult UitslagAanpassen()
        {
            List<IUitslag> uitslagen = UitslagFactory.MaakUitslagContainer().HaalAlleUitslagenOp();

            ViewBag.Uitslagen = uitslagen;

            return View();
        }

        [HttpPost]
        public IActionResult UitslagAanpassen(UitslagModel uitslagModel, string uitslagId)
        {
            List<IUitslag> uitslagen = UitslagFactory.MaakUitslagContainer().HaalAlleUitslagenOp();

            ViewBag.Uitslagen = uitslagen;


            string aanpasData = UitslagFactory.MaakUitslag(Convert.ToInt32(uitslagId), uitslagModel.Naam, uitslagModel.Datum, uitslagModel.ZetelAantal).UitslagAanpassen();

            ViewBag.AanpasData = aanpasData;

            return View();
        }
    }

}