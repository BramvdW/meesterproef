﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FactorysLogic;
using Meesterproef.Models;
using Microsoft.AspNetCore.Mvc;
using InterfacesLogic;

namespace Meesterproef.Controllers
{
    public class PartijController : Controller
    {
        public IActionResult PartijAanpassen()
        {
            List<IPartij> partijen = PartijFactory.MaakPartijContainerAan().HaalAllePartijenOp();

            ViewBag.partijen = partijen;

            
            return View();
        }

        [HttpPost]
        public IActionResult PartijAanpassen(PartijModel partijModel, int partijId)
        {
            List<IPartij> partijen = PartijFactory.MaakPartijContainerAan().HaalAllePartijenOp();

            ViewBag.partijen = partijen;

            string aanpasData = PartijFactory.MaakPartijAan(partijId, partijModel.Orde,partijModel.VoledigeNaam).PartijAanpassen();

            ViewBag.AanpasData = aanpasData;

            return View();
        }

        public IActionResult PartijToevoegen()
        {
            return View();
        }

        [HttpPost]
        public IActionResult PartijToevoegen(PartijModel partijModel)
        {

            IPartij iPartij = PartijFactory.MaakPartijAan(partijModel.Id, partijModel.Orde, partijModel.VoledigeNaam);
            string partijUitkomst = PartijFactory.MaakPartijContainerAan().MaakPartijAan(iPartij);

            ViewBag.PartijData = partijUitkomst;

            return View();
        }
    }
}