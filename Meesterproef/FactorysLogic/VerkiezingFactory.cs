﻿using System;
using System.Collections.Generic;
using System.Text;
using InterfacesLogic;
using LogicaLaag;

namespace FactorysLogic
{
    public static class VerkiezingFactory
    {
        public static IVerkiezing MaakVerkiezing(int id,int stemmen, decimal percentage, int zetels,string ordeTrekker,Partij partij)
        {
            IVerkiezing verkiezing = new Verkiezing(id,stemmen, percentage, zetels,ordeTrekker,partij);
            return verkiezing;
        }

        public static IVerkiezingContainer MaakVerkiezingContainer()
        {
            IVerkiezingContainer verkiezingContainer = new VerkiezingContainer();
            return verkiezingContainer;
        }
    }
}
