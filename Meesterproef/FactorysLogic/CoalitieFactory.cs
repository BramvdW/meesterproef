﻿using System;
using System.Collections.Generic;
using System.Text;
using InterfacesLogic;
using LogicaLaag;

namespace FactorysLogic
{
    public static class CoalitieFactory
    {
        public static ICoalitie MaakCoalitie(string naam)
        {
            ICoalitie coalitie = new Coalitie(naam);
            return coalitie;
        }

        public static ICoalitie MaakLegeCoalitie()
        {
            ICoalitie coalitie = new Coalitie();
            return coalitie;
        }

    }
}
