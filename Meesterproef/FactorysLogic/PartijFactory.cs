﻿using System;
using System.Collections.Generic;
using System.Text;
using InterfacesLogic;
using LogicaLaag;

namespace FactorysLogic
{
    public static class PartijFactory
    {
        public static IPartijContainer MaakPartijContainerAan()
        {
            IPartijContainer partijContainer = new PartijContainer();
            return partijContainer;
        }

        public static IPartij MaakPartijAan(int id, string orde, string volledigeNaam)
        {
            IPartij partij = new Partij(id,orde,volledigeNaam);
            return partij;
        }
    }
}
