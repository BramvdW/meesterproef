﻿using LogicaLaag;
using System;
using System.Collections.Generic;
using System.Text;
using InterfacesLogic;


namespace FactorysLogic
{
    public static class UitslagFactory
    {
        public static IUitslagContainer MaakUitslagContainer()
        {
            IUitslagContainer uitslagContainer = new UitslagContainer();
            return uitslagContainer;
        }

        public static IUitslag MaakUitslag(int id, string naam, DateTime datum, int zetelAantal)
        {
            IUitslag uitslagContainer = new Uitslag(id, naam, datum, zetelAantal);
            return uitslagContainer;
        }
    }
}
