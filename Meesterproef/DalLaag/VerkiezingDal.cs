﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using IntefacesDal;
using MySql.Data.MySqlClient;
using Structs;

namespace DalLaag
{
    public class VerkiezingDal : IVerkiezing,IVerkiezingContainer
    {
        private string connString = "server=localhost;database=meesterproef;user=root;password=;";

        MySqlConnection DbCon;

        public VerkiezingDTO HaalVerkiezingOp(int verkiezingId)
        {
            PartijDTO partijDto = new PartijDTO();
            string query = "SELECT `verkiezing`.`VerkiezingId`, `verkiezing`.`PartijId`, `verkiezing`.`UitslagId`, `verkiezing`.`Stemmen`, `verkiezing`.`Percentage`, `verkiezing`.`Zetels`, `verkiezing`.`OrdeTrekker`,`partij`.`PartijId`,`partij`.`Orde`,`partij`.`VoledigeNaam`" +
                           "FROM `verkiezing`" +
                           "INNER JOIN `partij`" +
                           "ON `verkiezing`.`PartijId` = `partij`.`PartijId`" +
                           "WHERE `verkiezing`.`VerkiezingId` = @verkiezingId";

            VerkiezingDTO verkiezingDto = new VerkiezingDTO();

            using (MySqlConnection DbCon = new MySqlConnection(connString))
            {
                MySqlCommand cmd = new MySqlCommand(query, DbCon);
                try
                {
                    DbCon.Open();

                    cmd.Parameters.AddWithValue("@verkiezingId", verkiezingId);

                    MySqlDataReader dataReader = cmd.ExecuteReader();
                    while (dataReader.Read())
                    {
                        VerkiezingDTO verkiezing = new VerkiezingDTO(
                            Convert.ToInt32(dataReader["VerkiezingId"]),
                            Convert.ToInt32(dataReader["Stemmen"]),
                            Convert.ToDecimal(dataReader["Percentage"]),
                            Convert.ToInt32(dataReader["Zetels"]),
                            dataReader["OrdeTrekker"].ToString(),
                            new PartijDTO(
                                Convert.ToInt32(dataReader["PartijId"]),
                                dataReader["Orde"].ToString(),
                                dataReader["VoledigeNaam"].ToString()
                            ));

                        verkiezingDto = verkiezing;
                    }
                    
                  
                    

                    return verkiezingDto;



                }
                catch (MySqlException ex)
                {
                    Debug.WriteLine(ex);
                }
                finally
                {
                    DbCon.Close();
                }

                return verkiezingDto;
            }
        }
    }
}
