﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using MySql.Data.MySqlClient;
using InterfacesDal;
using Structs;

namespace DalLaag
{

    public class UitslagDal : IUitslagContainer,IUitslag
    {
        private string connString = "server=localhost;database=meesterproef;user=root;password=;";

        MySqlConnection DbCon;

        public void MaakUitslagAan(UitslagDTO uitslag)
        {
            string query = "INSERT INTO `uitslag`( `Naam`, `Datum`, `ZetelAantal`) " +
                           "VALUES (@naam,@datum,@zetelaantal)";


            using (MySqlConnection DbCon = new MySqlConnection(connString))
            {
                MySqlCommand cmd = new MySqlCommand(query, DbCon);
                try
                {
                    DbCon.Open();

                    cmd.Parameters.AddWithValue("@naam", uitslag.Naam);
                    cmd.Parameters.AddWithValue("@datum", uitslag.Datum);
                    cmd.Parameters.AddWithValue("@zetelaantal", uitslag.ZetelAantal);

                    cmd.ExecuteReader();


                }
                catch (MySqlException ex)
                {
                    Debug.WriteLine(ex);
                }
                finally
                {
                    DbCon.Close();
                }

            }
        }

        public List<UitslagDTO> HaalAlleUitslagenOp()
        {
            string query = "SELECT `UitslagId`, `Naam`, `Datum`, `ZetelAantal`" +
                           "FROM `uitslag`";

            List<UitslagDTO> uitslagen = new List<UitslagDTO>();


            using (MySqlConnection DbCon = new MySqlConnection(connString))
            {
                MySqlCommand command = new MySqlCommand(query, DbCon);
                try
                {
                    DbCon.Open();


                    MySqlDataReader dataReader = command.ExecuteReader();

                    while (dataReader.Read())
                    {

                        UitslagDTO uitslag = new UitslagDTO(
                            Convert.ToInt32(dataReader["UitslagId"]),
                            dataReader["Naam"].ToString(),
                            Convert.ToDateTime(dataReader["Datum"]),
                            Convert.ToInt32(dataReader["ZetelAantal"]));



                        uitslagen.Add(uitslag);
                    }

                    return uitslagen;

                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex);
                }
                finally
                {
                    DbCon.Close();
                }

                return uitslagen;
            }
        }

        public UitslagDTO HaalAlleDataVanUitslagOp(UitslagDTO uitslag)
        {
            string query = "SELECT `verkiezing`.`VerkiezingId`,`verkiezing`.`UitslagId`, `verkiezing`.`Stemmen`, `verkiezing`.`Percentage`, `verkiezing`.`Zetels`, `partij`.`PartijId`, `partij`.`Orde`,`partij`.`VoledigeNaam`,`verkiezing`.`OrdeTrekker`" +
                           "FROM `verkiezing`" +
                           "INNER JOIN `partij` ON `verkiezing`.`PartijId` = `partij`.`PartijId`" +
                           "WHERE `verkiezing`.`UitslagId` = @uitslagId";
            
        
            using (MySqlConnection DbCon = new MySqlConnection(connString))
            {
                MySqlCommand command = new MySqlCommand(query, DbCon);
                try
                {
                    DbCon.Open();

                    command.Parameters.AddWithValue("@uitslagId", uitslag.Id);

                    MySqlDataReader dataReader = command.ExecuteReader();

                    

                    while (dataReader.Read())
                    {
                        VerkiezingDTO verkiezing = new VerkiezingDTO(
                            Convert.ToInt32(dataReader["VerkiezingId"]),
                            Convert.ToInt32(dataReader["Stemmen"]),
                            Convert.ToDecimal(dataReader["Percentage"]), 
                            Convert.ToInt32(dataReader["Zetels"]),
                            dataReader["OrdeTrekker"].ToString(),
                            new PartijDTO(
                                Convert.ToInt32(dataReader["PartijId"]),
                                dataReader["Orde"].ToString(),
                                dataReader["VoledigeNaam"].ToString()
                                ));

                        uitslag.Verkiezingen.Add(verkiezing);

                    }

                    return uitslag;

                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex);
                }
                finally
                {
                    DbCon.Close();
                }

                return uitslag;
            }
        }

        public UitslagDTO HaalUitslagOpMetNaam(string uitslagNaam)
        {
            string query = "SELECT `UitslagId`, `Naam`, `Datum`, `ZetelAantal` FROM `uitslag` WHERE `Naam` = @naam";


            UitslagDTO uitslag = new UitslagDTO();

            using (MySqlConnection DbCon = new MySqlConnection(connString))
            {
                MySqlCommand command = new MySqlCommand(query, DbCon);
                try
                {
                    DbCon.Open();

                    command.Parameters.AddWithValue("@naam", uitslagNaam);
                    

                    MySqlDataReader dataReader = command.ExecuteReader();

                    while (dataReader.Read())
                    {
                        uitslag = new UitslagDTO(
                            Convert.ToInt32(dataReader["UitslagId"]),
                            dataReader["Naam"].ToString(),
                            Convert.ToDateTime(dataReader["Datum"]),
                            Convert.ToInt32(dataReader["ZetelAantal"]));

                    }

                   

                    return uitslag;

                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex);
                }
                finally
                {
                    DbCon.Close();
                }

                return uitslag;
            }
        }

        public void UitslagAanpassen(UitslagDTO uitslag)
        {
            string query = "UPDATE `uitslag` " +
                           "SET `Naam`=@naam,`Datum`=@datum,`ZetelAantal`=@zetelAantal WHERE `UitslagId` = @uitslagId";


            using (MySqlConnection DbCon = new MySqlConnection(connString))
            {
                MySqlCommand cmd = new MySqlCommand(query, DbCon);
                try
                {
                    DbCon.Open();

                    cmd.Parameters.AddWithValue("@naam", uitslag.Naam);
                    cmd.Parameters.AddWithValue("@datum", uitslag.Datum);
                    cmd.Parameters.AddWithValue("@zetelaantal", uitslag.ZetelAantal);
                    cmd.Parameters.AddWithValue("@uitslagId", uitslag.Id);

                    cmd.ExecuteReader();

                }
                catch (MySqlException ex)
                {
                    Debug.WriteLine(ex);
                }
                finally
                {
                    DbCon.Close();
                }

            }
        }
    }
}
