﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using IntefacesDal;
using MySql.Data.MySqlClient;
using Structs;

namespace DalLaag
{
    public class PartijDal : IPartij,IPartijContainer
    {
        private string connString = "server=localhost;database=meesterproef;user=root;password=;";

        MySqlConnection DbCon;

        public List<PartijDTO> HaalAllePartijenOp()
        {
            string query = "SELECT `PartijId`, `Orde`, `VoledigeNaam`" +
                           "FROM `partij`";

            List<PartijDTO> partijen = new List<PartijDTO>();


            using (MySqlConnection DbCon = new MySqlConnection(connString))
            {
                MySqlCommand command = new MySqlCommand(query, DbCon);
                try
                {
                    DbCon.Open();


                    MySqlDataReader dataReader = command.ExecuteReader();

                    while (dataReader.Read())
                    {

                        PartijDTO partij = new PartijDTO(
                            Convert.ToInt32(dataReader["PartijId"]),
                            dataReader["Orde"].ToString(),
                            (dataReader["VoledigeNaam"].ToString()));



                        partijen.Add(partij);
                    }

                    return partijen;

                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex);
                }
                finally
                {
                    DbCon.Close();
                }

                return partijen;
            }
        }

        public void MaakPartijAan(PartijDTO partij)
        {
            string query = "INSERT INTO `partij`( `Orde`, `VoledigeNaam`) " +
                           "VALUES (@orde,@VoledigeNaam)";


            using (MySqlConnection DbCon = new MySqlConnection(connString))
            {
                MySqlCommand cmd = new MySqlCommand(query, DbCon);
                try
                {
                    DbCon.Open();

                    cmd.Parameters.AddWithValue("@orde", partij.Orde);
                    cmd.Parameters.AddWithValue("@VoledigeNaam", partij.VoledigeNaam);
                    

                    cmd.ExecuteReader();


                }
                catch (MySqlException ex)
                {
                    Debug.WriteLine(ex);
                }
                finally
                {
                    DbCon.Close();
                }

            }
        }

        public void PartijAanpassen(PartijDTO partijDto)
        {
            string query = "UPDATE `partij` " +
                           "SET `Orde`=@orde,`VoledigeNaam`=@volledigeNaam WHERE `PartijId` = @partijId";


            using (MySqlConnection DbCon = new MySqlConnection(connString))
            {
                MySqlCommand cmd = new MySqlCommand(query, DbCon);
                try
                {
                    DbCon.Open();

                    cmd.Parameters.AddWithValue("@orde", partijDto.Orde);
                    cmd.Parameters.AddWithValue("@volledigeNaam", partijDto.VoledigeNaam);
                    cmd.Parameters.AddWithValue("@partijId", partijDto.Id);

                    cmd.ExecuteReader();

                }
                catch (MySqlException ex)
                {
                    Debug.WriteLine(ex);
                }
                finally
                {
                    DbCon.Close();
                }

            }
        }
    }
}
