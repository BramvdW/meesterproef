﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Structs
{
    public struct VerkiezingDTO
    {
        public VerkiezingDTO(int id,int stemmen, decimal percentage, int zetels, string ordeTrekker, PartijDTO partij)
        {
            this.Id = id;
            this.Stemmen = stemmen;
            this.Percentage = percentage;
            this.Zetels = zetels;
            this.OrdeTrekker = ordeTrekker;
            this.Partij = partij;
        }


        public int Id { get; }
        public int Stemmen { get; }
        public decimal Percentage { get; }
        public int Zetels { get; }
        public string OrdeTrekker { get; }
        public PartijDTO Partij { get; }
    }
}
