﻿using System;
using System.Collections.Generic;

namespace Structs
{
    public struct UitslagDTO
    {
        public UitslagDTO(int id, string naam, DateTime datum, int zetelAantal, List<VerkiezingDTO> verkiezingen)
        {
            this.Id = id;
            this.Naam = naam;
            this.Datum = datum;
            this.ZetelAantal = zetelAantal;
            this.Verkiezingen = verkiezingen;
        }

        public UitslagDTO(int id, string naam, DateTime datum, int zetelAantal)
        {
            this.Id = id;
            this.Naam = naam;
            this.Datum = datum;
            this.ZetelAantal = zetelAantal;
            this.Verkiezingen = new List<VerkiezingDTO>();
        }

        public UitslagDTO(int v1, string v2, int v3, int v4) : this()
        {
        }

        public int Id { get; }

        public string Naam { get; }
        public DateTime Datum { get; }
        public int ZetelAantal { get; }

        public List<VerkiezingDTO> Verkiezingen {get; set; }

    }
}
