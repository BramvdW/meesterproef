﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Structs
{
    public struct PartijDTO
    {
        public PartijDTO(int Id,string orde, string volledigeNaam)
        {
            this.Id = Id;
            this.Orde = orde;
            this.VoledigeNaam = volledigeNaam;
            
        }
        public int Id { get;}
        public string Orde { get; }
        public string VoledigeNaam { get; }
        
    }
}
