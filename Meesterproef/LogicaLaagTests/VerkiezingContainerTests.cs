﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using LogicaLaag;
using System;
using System.Collections.Generic;
using System.Text;
using FactorysLogic;
using InterfacesLogic;

namespace LogicaLaag.Tests
{
    [TestClass()]
    public class VerkiezingContainerTests
    {
        [TestMethod()]
        public void HaalVerkiezingOpTest()
        {
            IVerkiezing verkiezing = VerkiezingFactory.MaakVerkiezingContainer().HaalVerkiezingOp(1);

            Assert.AreEqual("Persoon1", verkiezing.OrdeTrekker);
        }
    }
}