﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using LogicaLaag;
using System;
using System.Collections.Generic;
using System.Text;
using FactorysDal;
using InterfacesLogic;

namespace LogicaLaag.Tests
{
    [TestClass()]
    public class UitslagTests
    {
        [TestMethod()]
        public void BerekenZetelsVanVerkiezingenTest()
        {
            IVerkiezing verkiezing1 = new Verkiezing(1, 500, "OrdeTrekker1", new Partij(1, "test", "test"));
            IVerkiezing verkiezing2 = new Verkiezing(2, 1000, "OrdeTrekker2", new Partij(2, "test2", "test2"));

            List<IVerkiezing> verkiezingen = new List<IVerkiezing>();

            verkiezingen.Add(verkiezing1);
            verkiezingen.Add(verkiezing2);

            Uitslag uitslag = new Uitslag(1, "Uitslag 1", DateTime.Now, 150, verkiezingen);

            uitslag.BerekenZetelsVanVerkiezingen();



            Assert.AreEqual(50, verkiezing1.Zetels);
            Assert.AreEqual(100, verkiezing2.Zetels);

        }

        [TestMethod()]
        public void AlleStemmenOphalenUitUitslagTest()
        {
            IVerkiezing verkiezing1 = new Verkiezing(1, 500, "OrdeTrekker1", new Partij(1, "test", "test"));
            IVerkiezing verkiezing2 = new Verkiezing(2, 1000, "OrdeTrekker2", new Partij(2, "test2", "test2"));

            List<IVerkiezing> verkiezingen = new List<IVerkiezing>();

            verkiezingen.Add(verkiezing1);
            verkiezingen.Add(verkiezing2);

            Uitslag uitslag = new Uitslag(1, "Uitslag 1", DateTime.Now, 150, verkiezingen);


            Assert.AreEqual(1500, uitslag.AlleStemmen());
        }

        [TestMethod()]
        public void BerekenPercentageVanVerkiezingenTest()
        {
            IVerkiezing verkiezing1 = new Verkiezing(1, 500, "OrdeTrekker1", new Partij(1, "test", "test"));
            IVerkiezing verkiezing2 = new Verkiezing(2, 1000, "OrdeTrekker2", new Partij(2, "test2", "test2"));

            List<IVerkiezing> verkiezingen = new List<IVerkiezing>();

            verkiezingen.Add(verkiezing1);
            verkiezingen.Add(verkiezing2);

            Uitslag uitslag = new Uitslag(1, "Uitslag 1", DateTime.Now, 150, verkiezingen);

            uitslag.BerekenPercentageVanVerkiezingen();



            Assert.AreEqual(33, verkiezing1.Percentage);
            Assert.AreEqual(66, verkiezing2.Percentage);
        }

     
        //[TestMethod()]
        //public void UitslagAanpassenTest()
        //{
        //    UitslagFactory.MaakUitslag().UitslagAanpassen();
        //    Assert.Fail();
        //}
    }
}