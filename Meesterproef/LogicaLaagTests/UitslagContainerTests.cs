﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using LogicaLaag;
using System;
using System.Collections.Generic;
using System.Text;
using InterfacesLogic;
using Structs;
using FactorysLogic;

namespace LogicaLaag.Tests
{
    [TestClass()]
    public class UitslagContainerTests
    {
        [TestMethod()]
        public void HaalAlleDataVanUitslagOpTest()
        {
            IUitslag uitslagDto = new Uitslag(1, "Test32", DateTime.Parse("2014-03-31"), 150);



            IUitslag uitslag = UitslagFactory.MaakUitslagContainer().HaalAlleDataVanUitslagOp(uitslagDto);


            Assert.AreEqual("Test32", uitslag.Naam);
            Assert.AreEqual("test", uitslag.Verkiezingen[0].Partij.Orde);
            Assert.AreEqual("Persoon3", uitslag.Verkiezingen[0].OrdeTrekker);

        }

        [TestMethod]
        public void HaalAlleDataVanUitslagOpTest2()
        {
            IUitslag uitslagDto = new Uitslag(1, "Test32", DateTime.Parse("2014-03-31"), 150);


            IUitslag uitslag = UitslagFactory.MaakUitslagContainer().HaalAlleDataVanUitslagOp(uitslagDto);

            Assert.AreEqual("Nr4", uitslag.Verkiezingen[1].Partij.Orde);
            Assert.AreEqual("PartijNr4", uitslag.Verkiezingen[1].Partij.VoledigeNaam);

        }

        [TestMethod()]
        public void HaalUitslagOpMetNaamTest()
        {
            string uitslagNaam = "Verkiezingen2";

            IUitslag uitslagDto = UitslagFactory.MaakUitslagContainer().HaalUitslagOpMetNaam(uitslagNaam);

           
            Assert.AreEqual("Verkiezingen2", uitslagDto.Naam);
            Assert.AreEqual("4/4/1999 12:00:00 AM", uitslagDto.Datum.Date.ToString());
            Assert.AreEqual("150",uitslagDto.ZetelAantal.ToString());
        }

        public void HaalUitslagOpMetNaamTest2()
        {
            string uitslagNaam = "Verkiezingen2";

            IUitslag uitslagDto = UitslagFactory.MaakUitslagContainer().HaalUitslagOpMetNaam(uitslagNaam);


            Assert.AreEqual("Verkiezingen2", uitslagDto.Naam);
            Assert.AreEqual("4/4/1999 12:00:00 AM", uitslagDto.Datum.Date.ToString());
            Assert.AreNotEqual("", uitslagDto.ZetelAantal.ToString());
        }
    }
}
