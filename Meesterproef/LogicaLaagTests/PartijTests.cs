﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using LogicaLaag;
using System;
using System.Collections.Generic;
using System.Text;
using FactorysLogic;
using InterfacesLogic;

namespace LogicaLaag.Tests
{
    [TestClass()]
    public class PartijTests
    {
        [TestMethod()]
        public void PartijAanpassenTest()
        {
            string aanpasData = PartijFactory.MaakPartijAan(3, "TestOrde", "OrdeTestPartij").PartijAanpassen();


            Assert.AreEqual("Partij succesvol aangepast", aanpasData);
        }

        [TestMethod()]
        public void PartijAanpassenTestFail()
        {
            string aanpasData = PartijFactory.MaakPartijAan(3, "", "OrdeTestPartij").PartijAanpassen();


            Assert.AreEqual("Incorrecte afkorting", aanpasData);
        }
    }
}