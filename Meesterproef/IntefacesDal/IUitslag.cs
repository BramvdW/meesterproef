﻿using System;
using System.Collections.Generic;
using System.Text;
using Structs;

namespace InterfacesDal
{
    public interface IUitslag
    {
        void UitslagAanpassen(UitslagDTO uitslag);
    }
}
