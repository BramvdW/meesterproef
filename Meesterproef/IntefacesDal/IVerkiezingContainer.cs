﻿using System;
using System.Collections.Generic;
using System.Text;
using Structs;

namespace IntefacesDal
{
    public interface IVerkiezingContainer
    {
        VerkiezingDTO HaalVerkiezingOp(int verkiezingId);
    }
}
