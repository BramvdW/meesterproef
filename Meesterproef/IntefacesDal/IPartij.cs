﻿using System;
using System.Collections.Generic;
using System.Text;
using Structs;

namespace IntefacesDal
{
    public interface IPartij
    {
        void PartijAanpassen(PartijDTO partijDto);
    }
}
