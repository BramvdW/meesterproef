﻿using System;
using System.Collections.Generic;
using System.Text;
using Structs;

namespace IntefacesDal
{
    public interface IPartijContainer
    {
        void MaakPartijAan(PartijDTO partij);

        List<PartijDTO> HaalAllePartijenOp();
    }
}
