﻿using System;
using System.Collections.Generic;
using System.Text;
using FactorysDal;
using Structs;
using InterfacesLogic;

namespace LogicaLaag
{
    public class PartijContainer : IPartijContainer
    {

        public List<Partij> Partijen = new List<Partij>();

        public List<IPartij> HaalAllePartijenOp()
        {
            List<PartijDTO> partijenDtos = PartijFactory.MaakPartijContainerAan().HaalAllePartijenOp();

            List<IPartij> partijen = new List<IPartij>();

            foreach (PartijDTO dto in partijenDtos)
            {
                Partij partij = new Partij(dto.Id, dto.Orde, dto.VoledigeNaam);

                partijen.Add(partij);
            }

            return partijen;

        }

        public string MaakPartijAan(IPartij partij)
        {
            if (partij.Orde == null)
            {
                return "Incorrecte Orde";
            }else if (partij.VoledigeNaam == null)
            {
                return "Incorrecte Naam";
            }
            else
            {
                PartijDTO partijDto = new PartijDTO(partij.Id,partij.Orde,partij.VoledigeNaam);
                PartijFactory.MaakPartijContainerAan().MaakPartijAan(partijDto);
                return "Partij is succesvol aangemaakt";
            }
            
        }
    }
}
