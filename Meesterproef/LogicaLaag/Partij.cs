﻿using System;
using System.Collections.Generic;
using System.Text;
using FactorysDal;
using InterfacesLogic;
using Structs;

namespace LogicaLaag
{
    public class Partij : IPartij
    {
        public Partij(int id,string orde, string volledigeNaam)
        {
            this.Id = id;
            this.Orde = orde;
            this.VoledigeNaam = volledigeNaam;
        }

        public int Id { get; }
        public string Orde { get; }
        public string VoledigeNaam { get; }
        

        public string PartijAanpassen()
        {
            PartijDTO partijDto = new PartijDTO(Id, Orde, VoledigeNaam);

            if (partijDto.Orde == null || partijDto.Orde == "")
            {
                return "Incorrecte afkorting";
            }
            else if (partijDto.VoledigeNaam == null || partijDto.VoledigeNaam == "")
            {
                return "Incorrecte volledige naam";
            }
            else
            {
                PartijFactory.MaakPartij().PartijAanpassen(partijDto);
                return "Partij succesvol aangepast";
            }
        }

        public void VerwijderenPartij()
        {

        }
    }
}
