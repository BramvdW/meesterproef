﻿using System;
using System.Collections.Generic;
using System.Text;
using InterfacesLogic;

namespace LogicaLaag
{
    public class Coalitie : ICoalitie
    {

        public Coalitie(string naam)
        {
            this.Naam = naam;
        }

        public Coalitie()
        {
            verkiezingen = new List<IVerkiezing>();
        }

        public string Naam { get; }

        public List<IVerkiezing> verkiezingen { get; }

        public void ExportCoalitie()
        {

        }

        public void VoegVerkiezingToe(IVerkiezing verkiezing)
        {
            verkiezingen.Add(verkiezing);
        }
    }
}
