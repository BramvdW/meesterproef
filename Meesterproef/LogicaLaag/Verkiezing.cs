﻿using System;
using System.Collections.Generic;
using System.Text;
using InterfacesLogic;

namespace LogicaLaag
{
    public class Verkiezing : IVerkiezing
    {
        public Verkiezing(int id,int stemmen, decimal percentage, int zetels, string OrdeTrekker, Partij partij)
        {
            this.Id = id;
            this.Stemmen = stemmen;
            this.Percentage = percentage;
            this.Zetels = zetels;
            this.OrdeTrekker = OrdeTrekker;
            this.Partij = partij;
        }

        public Verkiezing(int id, int stemmen, string OrdeTrekker, Partij partij)
        {
            this.Id = id;
            this.Stemmen = stemmen;
            this.OrdeTrekker = OrdeTrekker;
            this.Partij = partij;
            
        }

        public int Id { get; }
        public int Stemmen { get; }
        public decimal Percentage { get; set; }
        public int Zetels { get; set; }
        public string OrdeTrekker { get; }
        public IPartij Partij { get; }


        public decimal BerekenPercentage(int stemmen)
        {
            return 0;
        }

        public int BerekenZetels(int stemmen)
        {
            return 0;
        }

    }
}
