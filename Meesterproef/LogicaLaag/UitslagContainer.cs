﻿using System;
using System.Collections.Generic;
using System.Text;
using FactorysDal;
using Structs;
using InterfacesLogic;

namespace LogicaLaag
{
    public class UitslagContainer : IUitslagContainer
    {
        public string MaakUitslagAan(IUitslag uitslag)
        {
            if (uitslag.Naam == null)
            {
                return "Incorrecte naam";
            }else if (uitslag.Datum.Date >= DateTime.Now.Date)
            {
                return "Incorrecte Datum";
            }else if (uitslag.ZetelAantal == 0)
            {
                return "Incorrecte aantal zetels";
            }
            else
            {

                UitslagDTO uitslagDto = new UitslagDTO(uitslag.Id,uitslag.Naam, uitslag.Datum, uitslag.ZetelAantal);

                UitslagFactory.MaakUitslagContainer().MaakUitslagAan(uitslagDto);
                return "Uitslag is succesvol aangemaakt";
            }
        }

        public List<IUitslag> HaalAlleUitslagenOp()
        {
            List<UitslagDTO> uitslagDtos = UitslagFactory.MaakUitslagContainer().HaalAlleUitslagenOp();
             
            List<IUitslag> Uitslagen = new List<IUitslag>();
            
            foreach (UitslagDTO dto in uitslagDtos)
            {
                Uitslag uitslag = new Uitslag(dto.Id,dto.Naam,dto.Datum,dto.ZetelAantal);

                Uitslagen.Add(uitslag);
            }

            return Uitslagen;

        }

        public IUitslag HaalAlleDataVanUitslagOp(IUitslag uitslag)
        {
            UitslagDTO uitslagdto = new UitslagDTO(uitslag.Id, uitslag.Naam, uitslag.Datum, uitslag.ZetelAantal);

            

            uitslagdto = UitslagFactory.MaakUitslagContainer().HaalAlleDataVanUitslagOp(uitslagdto);

            List<IVerkiezing> verkiezingen = new List<IVerkiezing>();

            foreach (VerkiezingDTO verkiezingDto in uitslagdto.Verkiezingen)
            {
                Partij partij = new Partij(verkiezingDto.Partij.Id,verkiezingDto.Partij.Orde, verkiezingDto.Partij.VoledigeNaam);

                IVerkiezing iVerkiezing = new Verkiezing(
                    verkiezingDto.Id,
                    verkiezingDto.Stemmen,
                    verkiezingDto.Percentage,
                    verkiezingDto.Zetels,
                    verkiezingDto.OrdeTrekker,
                    partij
                    );


                verkiezingen.Add(iVerkiezing);   
            }

            IUitslag iUitslag = new Uitslag(
                uitslagdto.Id,
                uitslagdto.Naam,
                uitslagdto.Datum,
                uitslagdto.ZetelAantal,
                verkiezingen);

            return iUitslag;
        }


        public IUitslag HaalUitslagOpMetNaam(string uitslagNaam)
        {
            UitslagDTO uitslagDto = UitslagFactory.MaakUitslagContainer().HaalUitslagOpMetNaam(uitslagNaam);

            IUitslag iUitslag = new Uitslag(uitslagDto.Id, uitslagDto.Naam, uitslagDto.Datum,uitslagDto.ZetelAantal);
            return iUitslag;
        }
    }
}
