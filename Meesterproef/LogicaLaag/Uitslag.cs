﻿using System;
using System.Collections.Generic;
using System.Text;
using FactorysDal;
using InterfacesLogic;
using Structs;

namespace LogicaLaag
{
    public class Uitslag : IUitslag
    {
        public Uitslag(int id, string naam, DateTime datum, int zetelAantal, List<IVerkiezing> verkiezingen)
        {
            this.Id = id;
            this.Naam = naam;
            this.Datum = datum;
            this.ZetelAantal = zetelAantal;
            this.Verkiezingen = verkiezingen;
        }

        public Uitslag(int id, string naam, DateTime datum, int zetelAantal)
        {
            this.Id = id;
            this.Naam = naam;
            this.Datum = datum;
            this.ZetelAantal = zetelAantal;
        }

        public int Id { get; }
        public string Naam { get; }
        public DateTime Datum { get; }
        public int ZetelAantal { get; }
        public List<IVerkiezing> Verkiezingen { get; }


        public void HaalVerkiezingenOp()
        {

        }


        public string UitslagAanpassen()
        {
            UitslagDTO uitslagDto = new UitslagDTO(Id,Naam,Datum,ZetelAantal);

            if (uitslagDto.Naam == null)
            {
                return "Incorrecte naam";
            }
            else if (uitslagDto.Datum.Date >= DateTime.Now.Date)
            {
                return "Incorrecte Datum";
            }
            else if (uitslagDto.ZetelAantal == 0)
            {
                return "Incorrecte aantal zetels";
            }
            else
            {
                UitslagFactory.MaakUitslag().UitslagAanpassen(uitslagDto);
                return "Uitslag succesvol aangepast";
            }

        }

        public int AlleStemmen()
        {
            int maxStemmen = 0;
            foreach (IVerkiezing verkiezing in Verkiezingen)
            {
                maxStemmen += verkiezing.Stemmen;
            }

            return maxStemmen;
        }

        public void BerekenPercentageVanVerkiezingen()
        {
            decimal procent = 0;
            int alleStemmmen = AlleStemmen();

            foreach (IVerkiezing verkiezing in Verkiezingen)
            {
                procent = ((decimal)verkiezing.Stemmen / (decimal)alleStemmmen) * (decimal)100;
                verkiezing.Percentage = Math.Floor(procent);
            }
        }

        public void BerekenZetelsVanVerkiezingen()
        {
            int alleStemmen = AlleStemmen();

            foreach (IVerkiezing verkiezing in Verkiezingen)
            {
                decimal verkiezingZetels = ((decimal)verkiezing.Stemmen / (decimal)alleStemmen) * (decimal)ZetelAantal;
                verkiezing.Zetels = Convert.ToInt32(verkiezingZetels);
            }
        }
    }
}
