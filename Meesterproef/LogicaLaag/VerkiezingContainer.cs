﻿using System;
using System.Collections.Generic;
using System.Text;
using FactorysDal;
using InterfacesLogic;
using Structs;

namespace LogicaLaag
{
    public class VerkiezingContainer : IVerkiezingContainer
    {
        public IVerkiezing HaalVerkiezingOp(int verkiezingId)
        {
            VerkiezingDTO verkiezing = VerkiezingFactory.MaakUitslagContainer().HaalVerkiezingOp(verkiezingId);

            IVerkiezing iVerkiezing = new Verkiezing(verkiezing.Id,verkiezing.Stemmen,verkiezing.Percentage, verkiezing.Zetels,verkiezing.OrdeTrekker,
                new Partij(verkiezing.Partij.Id,verkiezing.Partij.Orde,verkiezing.Partij.VoledigeNaam));
            return iVerkiezing;
        }
    }
}
